# Formation Git

Ces exercices sont des prétextes, afin de permettre de se concentrer sur Git. Il faut imaginer que les changements sont longs et compliqués !

## Énoncé

Récupérer le dépôt, et choisir le fichier à utiliser pour la suite en fonction de son langage préféré.
1. Résoudre le bug de manière à ce que le programme tourne. Ajouter les changements dans Git de manière à ce qu'ils soient prêts à être envoyés au serveur.
  Penser à utiliser une branche (attention, prétexte : en temps normal, ne pas créer de branche pour une modif aussi simple).
2. Écrire une fonction qui à x fait correspondre 2\*x + 5. La rendre ensuite plus générale, en permettant à l'utilisateur de fournir a et b tels que la fonction renvoie a\*x + b.
3. Fusionner les branches des questions précédentes dans `master`.
4. Fusionner la branche `later` dans `master`. Résoudre les conflits.
  À partir de cette question, les réponses ne sont plus données dans les slides. Documentez-vous sur internet, ou passez à net7 !
5. Changer le nom de la nouvelle fonction, par exemple en de super\_fonction à super\_affichage, _sans faire de nouveau commit_.
  On pourra se servir de `git rebase -i`, puis essayer une seconde méthode avec `git commit --fixup`.
6. Faire en sorte que la fonction factorielle affiche un message lorsqu'elle est exécutée, puis écrire une fonction qui à x fait correspondre x³. Ne pas faire de commit.
  Comment faire deux commits séparés une fois ces changements effectués ?
